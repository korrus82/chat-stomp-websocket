var stompClient = null;
var nickname = null;
var color = null;
var socket = null;

function setConnected(connected) {

    document.getElementById('connect').style.display = !connected ? '' : 'none';
    document.getElementById('disconnect').style.display = connected ? '' : 'none';
    document.getElementById('main-holder').style.display = connected ? '' : 'none';
    document.getElementById('nickname').disabled = connected;
    document.getElementById('color').disabled = connected;

}

function connect() {
    socket = new SockJS('/chat');
    stompClient = Stomp.over(socket);
    nickname = $("#nickname").val();
    color = $("#color").val();
    if (nickname != null & color != null) {
        stompClient.connect(nickname, color, function (frame) {
            setConnected(true);
            stompClient.subscribe('/topic/messages', function (message) {
                showMessages(JSON.parse(message.body));
            });
            stompClient.subscribe('/topic/receive', function (user) {
                showUser(JSON.parse(user.body));
            });
            var msg = {
                'nickname': nickname,
                'color': color
            };
            stompClient.send("/app/user", {}, JSON.stringify(msg));
        });
    } else {
        alert("Представьтесь и выберите цвет текста!");
    }
}

function disconnect() {
    var msg = {
        'nickname': nickname,
        'color': color
    };
    stompClient.send("/app/disconnect", {}, JSON.stringify(msg));
    stompClient.disconnect();
    setConnected(false);
}

function sendMessage() {
    var text = $("#text").val();
    if (text.length > 0) {
        var msg = {
            'nickname': nickname,
            'color': color,
            'text': text
        };
        stompClient.send("/app/chat", {}, JSON.stringify(msg));
        document.getElementById('text').value = null;
    }
}

function showMessages(message) {
    var response = document.getElementById('messages');
    var p = document.createElement('p');
    p.style.color = message.color;
    p.appendChild(document.createTextNode(message.date + " " + message.nickname + ": " + message.text));
    response.appendChild(p);
    response.scrollTop = response.scrollHeight;
}

function showUser(msgObj) {
    if (msgObj.length > 0) {
        $('.user').remove();
    }

    for (var i = 0; i < msgObj.length; i++) {
        renderUser(msgObj[i]);
    }
}

function renderUser(user) {
    var receive = document.getElementById('connected-users');
    var p = document.createElement('p');
    p.className = 'user';
    p.style.color = user.color;
    p.appendChild(document.createTextNode(user.nickname));
    receive.appendChild(p);
}