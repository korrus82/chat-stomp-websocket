package chat;

public class User implements Comparable<User>{
    private String nickname;
    private String color;

    public String getNickname() {
        return nickname;
    }

    public String getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (!color.equals(user.color)) return false;
        if (!nickname.equals(user.nickname)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nickname.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }

    @Override
    public int compareTo(User user) {
        if (equals(user)) {
            return 0;
        }

        int compareTo = getNickname().compareTo(user.getNickname());
        if (compareTo == 0) {
            return -1;
        }
        return compareTo;
    }
}