package chat;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Message {
    private String date;
    private String nickname;
    private String color;
    private String text;

    public String getDate() {
        Date now = new Date();
        String date = new SimpleDateFormat("HH:mm:ss").format(now);
        return date;
    }

    public String getNickname() {
        return nickname;
    }

    public String getColor() {
        return color;
    }

    public String getText() {
        return text;
    }

}
