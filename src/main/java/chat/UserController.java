package chat;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@Controller
public class UserController {
    final Set<User> userSet = Collections.synchronizedSet(new TreeSet<User>());

    // @todo : переделать на ApplicationListener<SessionSubscribeEvent> SessionUnsubscribeEvent SessionDisconnectEvent

    @MessageMapping("/user")
    @SendTo("/topic/receive")
    public Set<User> user(User user) throws Exception {
        userSet.add(user);
        return userSet;
    }

    @MessageMapping("/disconnect")
    @SendTo("/topic/receive")
    public Set<User> disconnect(User user) throws Exception {
        userSet.remove(user);
        return userSet;
    }
}
