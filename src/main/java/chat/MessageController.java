package chat;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController {


    @MessageMapping("/chat")
    @SendTo("/topic/messages")
    public Message message(Message message) throws Exception {
        return message;
    }
}


